# Shape variable dual LFO

This is a dual LFO synth module in Kosmo format, based on a [Kassutronics design](https://kassu2000.blogspot.com/2015/10/variable-waveshape-lfo.html). There are three outputs for each LFO. Depending on the shape pot setting these provide ramp, asymmetric triangle, symmetric triangle, or sawtooth waveforms; sine or skewed "sine" waveforms; and narrow, square, or wide pulse waveforms respectively.

Version 1.0 corrects some first run problems. Version 1.0 PCB has not been tested.

## Current draw
32 mA +12 V, 32 mA -12 V


## Photos

![front](Images/front.jpg)

![front](Images/back.jpg)

## Documentation

* [Schematic](Docs/kdlfo.pdf)
* PCB layout: [front](Docs/kdlfo_layout_front.pdf), [back](Docs/kdlfo_layout_back.pdf)
* [BOM](Docs/kdlfo_bom.md)
* [Build notes](Docs/build.md)
* [Blog post](https://analogoutputblog.wordpress.com/2022/05/11/kassutronics-dual-lfo/)

## Git repository

* [https://gitlab.com/rsholmes/duallfo](https://gitlab.com/rsholmes/kdlfo)


